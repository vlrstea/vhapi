-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for api
CREATE DATABASE IF NOT EXISTS `api` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `api`;

-- Dumping structure for table api.vh_history
CREATE TABLE IF NOT EXISTS `vh_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(6) DEFAULT NULL,
  `seat_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table api.vh_history: ~1 rows (approximately)
/*!40000 ALTER TABLE `vh_history` DISABLE KEYS */;
INSERT INTO `vh_history` (`id`, `action`, `seat_id`, `user_id`, `date`) VALUES
	(1, 'book', NULL, 12, '2018-10-14 18:55:06'),
	(2, 'cancel', 2, 12, '2018-10-14 21:07:28'),
	(3, 'book', NULL, 3, '2018-10-15 01:08:50'),
	(4, 'book', 10, 3, '2018-10-15 01:22:45'),
	(5, 'book', 9, 3, '2018-10-15 02:02:22'),
	(6, 'book', 13, 3, '2018-10-15 02:06:11');
/*!40000 ALTER TABLE `vh_history` ENABLE KEYS */;

-- Dumping structure for table api.vh_keys
CREATE TABLE IF NOT EXISTS `vh_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table api.vh_keys: ~0 rows (approximately)
/*!40000 ALTER TABLE `vh_keys` DISABLE KEYS */;
INSERT INTO `vh_keys` (`id`, `key`) VALUES
	(1, '26b8880c-0dc4-40d6-82c7-fe730796279b');
/*!40000 ALTER TABLE `vh_keys` ENABLE KEYS */;

-- Dumping structure for table api.vh_seats
CREATE TABLE IF NOT EXISTS `vh_seats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seat` char(2) NOT NULL,
  `booked` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table api.vh_seats: ~4 rows (approximately)
/*!40000 ALTER TABLE `vh_seats` DISABLE KEYS */;
INSERT INTO `vh_seats` (`id`, `seat`, `booked`, `user_id`, `date`) VALUES
	(1, 'A1', 0, 0, '2018-10-14 15:12:36'),
	(2, 'A2', 0, NULL, '2018-10-14 21:07:28'),
	(3, 'A3', 0, 0, '2018-10-14 15:13:02'),
	(4, 'A4', 0, 0, '2018-10-14 15:13:11'),
	(5, 'B1', 0, 0, '2018-10-14 15:13:11'),
	(6, 'B2', 1, 3, '2018-10-15 01:08:50'),
	(7, 'B3', 0, 0, '2018-10-14 15:13:11'),
	(8, 'B4', 0, 0, '2018-10-14 15:13:11'),
	(9, 'C1', 1, 3, '2018-10-15 02:02:22'),
	(10, 'C2', 1, 3, '2018-10-15 01:22:45'),
	(11, 'C3', 0, 0, '2018-10-15 00:43:56'),
	(12, 'C4', 0, 0, '2018-10-15 00:43:56'),
	(13, 'D1', 1, 3, '2018-10-15 02:06:11'),
	(14, 'D2', 0, 0, '2018-10-15 00:43:56'),
	(15, 'D3', 0, 0, '2018-10-15 00:43:56'),
	(16, 'D4', 0, 0, '2018-10-15 00:43:56'),
	(17, 'E1', 0, 0, '2018-10-15 00:43:56'),
	(18, 'E2', 0, 0, '2018-10-15 00:43:56'),
	(19, 'E3', 0, 0, '2018-10-15 00:43:56'),
	(20, 'E4', 0, 0, '2018-10-15 00:43:56'),
	(21, 'F1', 0, 0, '2018-10-15 00:43:56'),
	(22, 'F2', 0, 0, '2018-10-15 00:43:56'),
	(23, 'F3', 0, 0, '2018-10-15 00:43:56'),
	(24, 'F4', 0, 0, '2018-10-15 00:43:56'),
	(25, 'G1', 0, 0, '2018-10-15 00:43:56'),
	(26, 'G2', 0, 0, '2018-10-15 00:43:56'),
	(27, 'G3', 0, 0, '2018-10-15 00:43:56'),
	(28, 'G4', 0, 0, '2018-10-15 00:43:56'),
	(29, 'H1', 0, 0, '2018-10-15 00:43:56'),
	(30, 'H2', 0, 0, '2018-10-15 02:16:58'),
	(31, 'H3', 0, 0, '2018-10-15 02:16:58'),
	(32, 'H4', 0, 0, '2018-10-15 02:16:58');
/*!40000 ALTER TABLE `vh_seats` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
