<?php
require_once('vendor/autoload.php');

use app\config\DB as DB;
use app\Response as Response;
use app\models\Request as Request;
use app\RequestedPage;


//get the requested page
$requestedPage = new RequestedPage( $_SERVER['REQUEST_URI'] );
$requestedPage = $requestedPage->get();
$requestedMethod = $_SERVER['REQUEST_METHOD'];


$response = new Response;
$db = new DB;

if($db->connection()){

	$request = new Request( $db );


//check if the apikey parameter is set and if is valid
	if( isset( $_REQUEST['api_key'] ) && $request->checkAPIKey( htmlspecialchars( $_REQUEST['api_key'] ) )){
	//routing(kind of :D)
		switch($requestedPage){


		//get all seats with info
			case 'seats':

			if($requestedMethod != 'GET'){
				$response->withStatusCode(405);
				break;
			}

			$response->withJson( $request->all()->results() );

			break;

		//get the availalbe seats
			case 'available':

			if($requestedMethod != 'GET'){
				$response->withStatusCode(405);
				break;
			}

			$response->withJson( $request->availableSeats()->results() );

			break;
		//book a seat
			case 'book':

			if($requestedMethod != 'POST'){
				$response->withStatusCode(405);
				break;
			}

		//if seat_rand argument is set int he URI, and it is false, set $request->rand as false;
			$request->rand = (!isset($_GET['seat_rand']) || $_GET['seat_rand'] == 'false') ? false : true;
			$request->seat_id = ($request->rand === true) ? $request->availableSeats(true, 'id')->first()['id'] : (int) $_GET['seat_id'];
			$request->client_id = (int)$_GET['client_id'];
			$request->action = 'book';

			$response->withJson( $request->book() );

			break;

			case 'cancel':

			if( $requestedMethod != 'POST' ){
				$response->withStatusCode(405);
				break;
			}

		//Parse the URL arguments
			$request->seat_id = (int) $_GET['seat_id'];
			$request->client_id = (int)$_GET['client_id'];
			$request->action = 'cancel';

			$response->withJson( $request->cancel() );

			break;

			default:
			$response->withStatusCode(404);
			break;
		}



	} else {

		$response->withStatusCode(401);
	}




} else {

	$response->withStatusCode(503);
}





//set the headers and return the response
header( sprintf('HTTP/%s %s %s',

	'1.1',
	$response->getStatusCode(),
	''

) );


foreach ($response->getHeaders() as $header) {

	header( $header[0] . ':' . $header[1]);
}

echo $response->getBody();

