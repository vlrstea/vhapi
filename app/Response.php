<?php
namespace app;

class Response {	

	protected $body,
			  $statusCode = 200,
			  $headers = [];

	
	public function withBody( $body ){

		$this->body = $body;
		return $this;
	}

	public function getBody(){
		return $this->body;
	}



	public function withJson( $body ){

		$this->withHeader( 'Content-Type', 'application/json' );	
		$this->body = json_encode( $body );

		return $this;
	}



	public function withHeader( $name, $value ){

		$this->headers[] = [ $name, $value ];
		return $this;
	}

	public function getHeaders(){
		return $this->headers;
	}

	public function withStatusCode( int $statusCode ){
		$this->statusCode = $statusCode;
	}

	public function getStatusCode(){
		return $this->statusCode;
	}
	
}