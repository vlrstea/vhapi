<?php
namespace app\models;


class Request{

	public    $id,
			  $action,
			  $client_id,
			  $seat_id,
			  $rand;


	private $db;

	public function __construct( $db ){
		
		$this->db = $db;

	}

	public function all(){
		
		return $this->db->query( "SELECT id, seat, booked FROM vh_seats" );

	}


	//Get a list with the available seats
	public function availableSeats( $rand = false, $field = 'id, seat'){

		$query = "SELECT $field FROM vh_seats WHERE booked = 0";

		//if seat_rand is true
		if($rand){

			$query .= " ORDER BY rand() LIMIT 1";
		}

		return $this->db->query( $query );

		
	}

	//Book a seat
	public function book(){

		try{
		
			$this->db->connection->beginTransaction();
		
				// //First query
				$query1 = "SELECT id FROM vh_seats WHERE id = :seatid AND booked = 0";
				$stmt = $this->db->connection->prepare( $query1 );
				$stmt->execute([':seatid' => $this->seat_id]);
				$stmt->closeCursor();
			
				if($stmt->rowCount() == 0){
					return false;
				}


				//Second query
				$query2 = "UPDATE vh_seats SET booked = 1, user_id = :userid WHERE id = :seatid";

				$stmt = $this->db->connection->prepare($query2);
				$stmt->execute(['userid' => $this->client_id, ':seatid' => $this->seat_id]);
				$stmt->closeCursor();


				//Third query
				$query3 = "INSERT INTO vh_history (action, seat_id, user_id) VALUES( :action, :seatid, :userid )";

				$stmt = $this->db->connection->prepare($query3);
				$stmt->execute([
					':action' => $this->action,
					':seatid' => $this->seat_id, 
					':userid' => $this->client_id
				]);

			$this->db->connection->commit();

			return true;

		} catch(PDOException $e){

			echo 'Error ' . $e->getMessage();

			return false;

		}

	}

	public function cancel(){

		
		try{
		
			$this->db->connection->beginTransaction();


				//First query
				$query1 = "SELECT id FROM vh_seats WHERE user_id = :userid AND id = :seatid AND booked = 1";
				$stmt = $this->db->connection->prepare($query1);
				$stmt->execute([

					':seatid' => $this->seat_id,
					':userid' => $this->client_id

				]);

				$stmt->closeCursor();

				if($stmt->rowCount() == 0){
					return false;
				}


				//Second query
				$query2 = "UPDATE vh_seats SET booked = 0, user_id = NULL WHERE id = :seatid";

				$stmt = $this->db->connection->prepare($query2);
				$stmt->execute([
					':seatid' => $this->seat_id
				]);

				$stmt->closeCursor();


				//Third query
				$query3 = "INSERT INTO vh_history (action, seat_id, user_id) VALUES( :action, :seatid, :userid )";

				$stmt = $this->db->connection->prepare($query3);

				$stmt->execute([
					':action' => $this->action,
					':seatid' => $this->seat_id, 
					':userid' => $this->client_id,	
				]);

			$this->db->connection->commit();

			return true;

		} catch(PDOException $e){

			echo 'Error ' . $e->getMessage();

			return false;

		}

	}


	//check if the provided apikey is valid
	public function checkAPIKey( string $key){
		
		if(strlen($key) == 36 ){

			$query = "SELECT `id` FROM vh_keys WHERE `key` = ? LIMIT 0,1";
			$queryResult = $this->db->query($query, [$key]);

			if($queryResult->results()){

				return true;
				
			} else {

				return false;
			}
		}
	}

}