<?php 
namespace app\config;
use \PDO;
//use \PDOException;

class DB{

	protected $host = '127.0.0.1',
			  $port = '33061',
			  $dbname = 'api',
			  $username = 'root',
			  $password = '',
			  $query,
			  $results,
			  $count,
			  $first;


	public $connection;


	public function __construct(){
		
				try{

			$this->connection = new PDO('mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->dbname, $this->username, $this->password);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


		} catch(\PDOException $e){

			//echo 'Connection Error ' . $e->getMessage();
			return false;
		}

	}

	public function query( $sql, $params = array() ){
		
		if($this->query = $this->connection->prepare( $sql )){
			
			if(count($params)){

				$num = 1;

				foreach($params as $param){
					$this->query->bindParam($num, $param);
					$num++;
				}	
			}


			if($this->query->execute()){

				$this->results = $this->query->fetchAll(PDO::FETCH_ASSOC);
				$this->count = $this->query->rowCount();

			} else {

				return false;
			}

			return $this;
		}

	}

	public function results(){
		return $this->results;
	}

	public function first(){
		return $this->results[0];
	}

	public function connection(){
		return $this->connection;
	}


}